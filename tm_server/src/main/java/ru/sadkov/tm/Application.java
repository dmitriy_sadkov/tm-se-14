package ru.sadkov.tm;


import ru.sadkov.tm.api.ServiceLocator;
import ru.sadkov.tm.dto.SessionDTO;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.launcher.Bootstrap;
import ru.sadkov.tm.util.ListShowUtil;
import ru.sadkov.tm.util.RandomUtil;


public class Application {

    public static void main(String[] args) {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        new Bootstrap().init();

//        ServiceLocator boot = new Bootstrap();
//        boot.getProjectService().clear();
//        boot.getUserService().clear();
//        User user = new User("aa","aa", Role.ADMIN);
//        User admin = new User("admin","admin",Role.USER);
//        boot.getUserService().userRegister(user);
//        boot.getUserService().userRegister(admin);
//        SessionDTO session = boot.getSessionService().open("aa","aa");
//        Project project = new Project("new","new",user,RandomUtil.UUID());
//        boot.getProjectService().persist(project);
//        Project project1 = new Project("newww","newww",user, RandomUtil.UUID());
//        boot.getProjectService().persist(project1);
//        ListShowUtil.showList(boot.getProjectService().findAll(user));
//        boot.getSessionService().close(session);
        }
    }


