package ru.sadkov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.enumeration.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tasks")
public final class Task extends AbstractEntity {

    @NotNull
    @Column(name = "name")
    private String name;

    @Nullable
    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "date_create")
    @Temporal(TemporalType.DATE)
    private Date dateCreate;

    @Nullable
    @Column(name = "date_begin")
    @Temporal(TemporalType.DATE)
    private Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    @Temporal(TemporalType.DATE)
    private Date dateEnd;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Status status;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    private Project project;

    public Task(@NotNull final String id, @NotNull final String name, @NotNull final User user, @NotNull final Project project) {
        this.id = id;
        this.name = name;
        this.description = "empty";
        this.user = user;
        this.project = project;
        this.dateEnd = new Date();
        this.dateBegin = new Date();
        this.dateCreate = new Date();
    }
}
