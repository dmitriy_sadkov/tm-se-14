package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.*;
import ru.sadkov.tm.repository.ProjectRepository;
import ru.sadkov.tm.dto.ProjectDTO;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;
import ru.sadkov.tm.util.RandomUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public final class ProjectService extends AbstractService implements IProjectService {

    @NotNull
    final private ServiceLocator serviceLocator;

    public ProjectService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public List<Project> getSortedProjectList(@Nullable final String userId, @Nullable final Comparator<Project> comparator) {
        if (userId == null || userId.isEmpty()) return null;
        if (comparator == null) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @Nullable final IProjectRepository projectDAO = new ProjectRepository(manager);
        @NotNull final List<Project> projects = projectDAO.findAll(userId);
        projects.sort(comparator);
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectDTO> convertList(@Nullable final List<Project> projects) {
        @NotNull final List<ProjectDTO> result = new ArrayList<>();
        if (projects == null) return result;
        for (@NotNull final Project project : projects) {
            result.add(convert(project));
        }
        return result;
    }

    @NotNull
    @Override
    public ProjectDTO convert(@Nullable final Project project) {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        if (project == null) return projectDTO;
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateBegin(project.getDateBegin());
        projectDTO.setDateCreate(project.getDateCreate());
        projectDTO.setDateEnd(project.getDateEnd());
        projectDTO.setStatus(project.getStatus());
        projectDTO.setUserId(project.getUser().getId());
        return projectDTO;
    }

    @Nullable
    public String findProjectIdByName(@Nullable final String projectName, @Nullable final String userId) {
        if (projectName == null || projectName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @Nullable final IProjectRepository projectDAO = new ProjectRepository(manager);
        @Nullable final Project project = projectDAO.findProjectByName(projectName, userId);
        if (project == null) return null;
        return project.getId();
    }

    public boolean persist(@Nullable final String projectName, @Nullable final String userId, @Nullable final String description) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        if (description == null || description.isEmpty()) return false;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return false;
        @Nullable final IProjectRepository projectDAO = new ProjectRepository(manager);
        if (projectDAO.containsByName(userId, projectName)) return false;
        @NotNull final Project project = new Project(projectName, description, serviceLocator.getUserService().findOneById(userId), RandomUtil.UUID());
        projectDAO.persist(project);
        return true;

    }

    public void removeByName(@Nullable final String projectName, @Nullable final String userId) {
        if (projectName == null || projectName.isEmpty() || userId == null || userId.isEmpty()) return;
        @Nullable final String projectId = findProjectIdByName(projectName, userId);
        if (projectId == null || projectId.isEmpty()) return;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @Nullable final IProjectRepository projectDAO = new ProjectRepository(manager);
        projectDAO.remove(userId, projectName);
    }

    public @Nullable List<Project> findAll(@Nullable final User user) {
        if (user == null) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @Nullable final IProjectRepository projectDAO = new ProjectRepository(manager);
        return projectDAO.findAll(user.getId());
    }

    public void removeAll(@Nullable final User user) {
        if (user == null) return;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @Nullable final IProjectRepository projectDAO = new ProjectRepository(manager);
        projectDAO.removeAll(user.getId());
    }

    public void update(@Nullable final String oldName, @Nullable final String newName, @Nullable final String description, @Nullable final String userId) {
        if (oldName == null || newName == null || oldName.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @Nullable final IProjectRepository projectDAO = new ProjectRepository(manager);
        @Nullable final Project project = projectDAO.findProjectByName(oldName, userId);
        if (project == null) return;
        projectDAO.update(project.getUser().getId(), project.getId(), description, newName);
    }

    public @Nullable Project findOneByName(@Nullable final String projectName, @Nullable final User currentUser) {
        if (projectName == null || projectName.isEmpty() || currentUser == null) return null;
        @Nullable final String projectId = findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @Nullable final IProjectRepository projectDAO = new ProjectRepository(manager);
        return projectDAO.findProjectByName(projectName, currentUser.getId());
    }

    @Override
    public @Nullable List<Project> findProjectsByPart(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || userId.isEmpty()) return null;
        if (part == null || part.isEmpty()) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @Nullable final IProjectRepository projectDAO = new ProjectRepository(manager);
        return projectDAO.findProjectsByPart(userId, part);
    }

    @Override
    public @Nullable List<Project> findProjectsByStatus(@Nullable final String userId, @Nullable final Status status) {
        if (userId == null || userId.isEmpty() || status == null) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @Nullable final IProjectRepository projectDAO = new ProjectRepository(manager);
        return projectDAO.findProjectsByStatus(userId, status);
    }

    @Override
    @Nullable
    public String startProject(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectName == null || projectName.isEmpty()) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @Nullable final IProjectRepository projectDAO = new ProjectRepository(manager);
        @NotNull final Date startDate = new Date();
        projectDAO.startProject(userId, projectName, startDate);
        return simpleDateFormat.format(startDate);
    }

    @Override
    public @Nullable String endProject(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectName == null || projectName.isEmpty()) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @Nullable final IProjectRepository projectDAO = new ProjectRepository(manager);
        @NotNull final Date endDate = new Date();
        projectDAO.startProject(userId, projectName, endDate);
        return simpleDateFormat.format(endDate);
    }

    @Override
    public @NotNull List<Project> findAll() {
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @Nullable final IProjectRepository projectDAO = new ProjectRepository(manager);
        return projectDAO.findAll();
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @Nullable final IProjectRepository projectDAO = new ProjectRepository(manager);
        projectDAO.persist(project);
    }

    @Override
    public void clear() {
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @Nullable final IProjectRepository projectDAO = new ProjectRepository(manager);
        projectDAO.clear();
    }

    @Override
    public void load(@Nullable final List<Project> projects) {
        if (projects == null) return;
        clear();
        for (@NotNull final Project project : projects) {
            persist(project);
        }
    }
}
