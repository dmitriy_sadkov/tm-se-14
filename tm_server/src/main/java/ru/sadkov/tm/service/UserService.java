package ru.sadkov.tm.service;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IUserRepository;
import ru.sadkov.tm.api.IUserService;
import ru.sadkov.tm.api.ServiceLocator;
import ru.sadkov.tm.repository.UserRepository;
import ru.sadkov.tm.dto.UserDTO;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.util.HashUtil;
import ru.sadkov.tm.util.RandomUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public final class UserService extends AbstractService implements IUserService {

    @NotNull
    final private ServiceLocator serviceLocator;

    public UserService(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void userRegister(@Nullable final User user) {
        if (user == null) return;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return;
        if (user.getPassword() == null || user.getPassword().isEmpty()) return;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @Nullable final IUserRepository userDAO = new UserRepository(manager);
        user.setId(RandomUtil.UUID());
        user.setPassword(HashUtil.hashMD5(user.getPassword()));
        userDAO.persist(user);
    }

    public void userAddFromData(@Nullable final User user) {
        if (user == null) return;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @Nullable final IUserRepository userDAO = new UserRepository(manager);
        userDAO.persist(user);
    }


    @Override
    public void userRegister(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @Nullable final IUserRepository userDAO = new UserRepository(manager);
        @NotNull final User user = new User(login, password, Role.USER);
        user.setId(RandomUtil.UUID());
        user.setPassword(HashUtil.hashMD5(user.getPassword()));
        userDAO.persist(user);
    }

    public boolean login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return false;
        @Nullable final IUserRepository userDAO = new UserRepository(manager);
        @Nullable final User user = userDAO.findByLogin(login);
        if (user == null || user.getPassword() == null) return false;
        return user.getPassword().equals(HashUtil.hashMD5(password));
    }

    @Override
    public @NotNull List<User> findAll() {
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return new ArrayList<>();
        @Nullable final IUserRepository userDAO = new UserRepository(manager);
        return userDAO.findAll();
    }

    @Override
    @NotNull
    public UserDTO convert(@Nullable final User user) {
        @NotNull final UserDTO userDTO = new UserDTO();
        if (user == null) return userDTO;
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPassword(user.getPassword());
        userDTO.setRole(user.getRole());
        return userDTO;
    }

    @Override
    @NotNull
    public List<UserDTO> convertList(@Nullable final List<User> users) {
        @NotNull final List<UserDTO> result = new ArrayList<>();
        if (users == null) return result;
        for (@NotNull final User user : users) {
            result.add(convert(user));
        }
        return result;
    }

    public void addTestUsers() {
        @NotNull final User admin = new User("admin", "admin", Role.ADMIN);
        @NotNull final User user = new User("user", "user", Role.USER);
        userRegister(admin);
        userRegister(user);
    }

    @Override
    public void clear() {
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @Nullable final IUserRepository userDAO = new UserRepository(manager);
        userDAO.clear();
    }

    @Override
    public void load(@Nullable final List<User> users) {
        if (users == null) return;
        clear();
        for (@NotNull final User user : users) {
            userRegister(user);
        }
    }

    @Override
    @Nullable
    public User findOneById(@NotNull String userId) {
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @Nullable final IUserRepository userDAO = new UserRepository(manager);
        return userDAO.findOne(userId);
    }

    @Override
    @Nullable
    public User findOneByLogin(@Nullable final String login) {
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @Nullable final IUserRepository userDAO = new UserRepository(manager);
        return userDAO.findByLogin(login);
    }
}
