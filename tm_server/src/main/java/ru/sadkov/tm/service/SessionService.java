package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.sadkov.tm.api.ISessionRepository;
import ru.sadkov.tm.api.ISessionService;
import ru.sadkov.tm.api.ServiceLocator;
import ru.sadkov.tm.repository.SessionRepository;
import ru.sadkov.tm.dto.SessionDTO;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.exception.AccessForbiddenException;


import ru.sadkov.tm.util.RandomUtil;
import ru.sadkov.tm.util.SignatureUtil;


import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public final class SessionService implements ISessionService {

    @NotNull
    final private ServiceLocator serviceLocator;

    public SessionService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public SessionDTO open(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @Nullable final ISessionRepository sessionDAO = new SessionRepository(manager);
        @Nullable Session session;
        if (serviceLocator.getUserService().login(login, password)) {
            @NotNull SessionDTO sessionDTO = new SessionDTO();
            @Nullable final User user = serviceLocator.getUserService().findOneByLogin(login);
            if (user == null) return null;
            sessionDTO.setId(RandomUtil.UUID());
            sessionDTO.setRole(user.getRole());
            sessionDTO.setTimeStamp(System.currentTimeMillis());
            sessionDTO.setUserId(user.getId());
            sessionDTO = sign(sessionDTO);
            session = new Session();
            session.setTimeStamp(sessionDTO.getTimeStamp());
            session.setUser(user);
            session.setId(sessionDTO.getId());
            session.setRole(sessionDTO.getRole());
            session.setSignature(sessionDTO.getSignature());
            sessionDAO.persist(session);
            return sessionDTO;
        }
        return null;
    }

    @Override
    public void validate(@Nullable final SessionDTO session) throws Exception {
        if (session == null) throw new Exception();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessForbiddenException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessForbiddenException();
        if (session.getTimeStamp() == null) throw new AccessForbiddenException();
        if (session.getRole() == null) throw new AccessForbiddenException();
        @Nullable final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessForbiddenException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTemp = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTemp);
        if (!check) throw new Exception();
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) throw new Exception();
        @Nullable final ISessionRepository sessionDAO = new SessionRepository(manager);
        @Nullable final Session ses = sessionDAO.findOne(session.getId());
        if (ses == null) throw new AccessForbiddenException();
        if (!sessionDAO.contains(ses)) throw new AccessForbiddenException();
    }

    @Nullable
    private SessionDTO sign(@Nullable final SessionDTO session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final String salt = serviceLocator.getPropertyService().getSessionSalt();
        @NotNull final Integer cycle = serviceLocator.getPropertyService().getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        if (signature == null || signature.isEmpty()) return null;
        session.setSignature(signature);
        return session;
    }

    @Override
    public void validate(@Nullable final SessionDTO session, @Nullable final Role role) throws Exception {
        if (session == null || role == null) throw new Exception();
        validate(session);
        @NotNull final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findOneById(userId);
        if (user == null) throw new AccessForbiddenException();
        final boolean check = user.getRole().equals(role);
        if (!check) throw new AccessForbiddenException();
    }

    @Override
    public boolean isValid(@Nullable final SessionDTO session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void close(@Nullable final SessionDTO session) {
        if (session == null) return;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @Nullable final ISessionRepository sessionDAO = new SessionRepository(manager);
        @Nullable final Session ses = sessionDAO.findOne(session.getId());
        if (ses == null) return;
        sessionDAO.close(ses);
    }

    @Override
    public void closeAll() {
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @Nullable final ISessionRepository sessionDAO = new SessionRepository(manager);
        sessionDAO.clear();
    }

    @Override
    public List<Session> getListSession(@Nullable final SessionDTO session) {
        if (session == null) return new ArrayList<>();
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return new ArrayList<>();
        @Nullable final ISessionRepository sessionDAO = new SessionRepository(manager);
        return sessionDAO.findByUserId(session.getUserId());
    }

    @Override
    public User getUser(@NotNull final SessionDTO session) {
        @Nullable final User user = serviceLocator.getUserService().findOneById(session.getUserId());
        return user;
    }

    @Override
    public void load(@Nullable final List<Session> sessions) {
        if (sessions == null) return;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @Nullable final ISessionRepository sessionDAO = new SessionRepository(manager);
        sessionDAO.clear();
        for (@NotNull final Session session : sessions) {
            sessionDAO.persist(session);
        }
    }

    @Override
    @NotNull
    public List<Session> findAll() {
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return new ArrayList<>();
        @Nullable final ISessionRepository sessionDAO = new SessionRepository(manager);
        @Nullable final List<Session> sessions = sessionDAO.findAll();
        if (sessions == null) return new ArrayList<>();
        return sessions;
    }
}
