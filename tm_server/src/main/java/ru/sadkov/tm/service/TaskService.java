package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IProjectService;

import ru.sadkov.tm.api.ITaskRepository;
import ru.sadkov.tm.api.ITaskService;

import ru.sadkov.tm.api.ServiceLocator;
import ru.sadkov.tm.repository.TaskRepository;
import ru.sadkov.tm.dto.TaskDTO;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;

import ru.sadkov.tm.util.RandomUtil;


import javax.persistence.EntityManager;
import java.util.*;

public final class TaskService extends AbstractService implements ITaskService {

    @NotNull
    private IProjectService projectService;

    @NotNull
    final private ServiceLocator serviceLocator;


    public TaskService(@NotNull final IProjectService projectService, @NotNull final ServiceLocator serviceLocator) {
        this.projectService = projectService;
        this.serviceLocator = serviceLocator;
    }

    public @Nullable Task findTaskByName(@Nullable final String taskName, @Nullable final String userId) {
        if (taskName == null || taskName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        return taskDAO.findTaskByName(taskName, userId);
    }

    @NotNull
    @Override
    public List<TaskDTO> convertList(@Nullable final List<Task> tasks) {
        @NotNull final List<TaskDTO> result = new ArrayList<>();
        if (tasks == null) return result;
        for (@NotNull final Task task : tasks) {
            result.add(convert(task));
        }
        return result;
    }

    @NotNull
    @Override
    public TaskDTO convert(@Nullable final Task task) {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        if (task == null) return taskDTO;
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDateBegin(task.getDateBegin());
        taskDTO.setDateCreate(task.getDateCreate());
        taskDTO.setDateEnd(task.getDateEnd());
        taskDTO.setStatus(task.getStatus());
        taskDTO.setUserId(task.getUser().getId());
        taskDTO.setProjectId(task.getProject().getId());
        return taskDTO;
    }

    @Nullable
    public List<Task> getSortedTaskList(@Nullable final String userId, @Nullable final Comparator<Task> comparator) {
        if (userId == null || userId.isEmpty()) return null;
        if (comparator == null) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        @NotNull final List<Task> tasks = taskDAO.findAll(userId);
        tasks.sort(comparator);
        return tasks;
    }

    public boolean saveTask(@Nullable final String taskName, @Nullable final String projectName, @Nullable final User currentUser) {
        if (taskName == null || taskName.isEmpty()) return false;
        if (projectName == null || projectName.isEmpty()) return false;
        if (currentUser == null) return false;
        @Nullable final String projectId = projectService.findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return false;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return false;
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        if (taskDAO.containsByName(currentUser.getId(), taskName)) return false;
        @NotNull final Task task = new Task(RandomUtil.UUID(), taskName, currentUser, projectService.findOneByName(projectName, currentUser));
        persist(task);
        return true;
    }

    public void removeTask(@Nullable final String taskName, @Nullable final User currentUser) {
        if (taskName == null || taskName.isEmpty()) return;
        if (currentUser == null) return;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        taskDAO.removeByName(taskName, currentUser.getId());
    }

    @Nullable
    public List<Task> findAll(@Nullable final User user) {
        if (user == null) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        return taskDAO.findAll(user.getId());
    }


    public void removeAll(@Nullable final User user) {
        if (user == null) return;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        taskDAO.removeAll(user.getId());
    }

    public void removeTaskForProject(@Nullable final String projectName, @Nullable final User currentUser) {
        if (projectName == null || projectName.isEmpty() || currentUser == null) return;
        @Nullable final String projectId = projectService.findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        @Nullable final Project project = projectService.findOneByName(projectName, currentUser);
        if (project == null) return;
        @NotNull final Iterator<Task> iterator = taskDAO.findAll(currentUser.getId()).iterator();
        while (iterator.hasNext()) {
            @NotNull final Task task = iterator.next();
            if (task.getProject().getId().equals(projectId)) {
                taskDAO.removeByName(task.getName(), currentUser.getId());
            }
        }
    }

    public void update(@Nullable final String oldName, @Nullable final String newName, @Nullable final String userId) {
        if (oldName == null || oldName.isEmpty()) return;
        if (newName == null || newName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        @Nullable final Task task = taskDAO.findTaskByName(oldName, userId);
        if (task == null) return;
        taskDAO.update(task.getId(), newName, userId);
    }

    @Override
    @Nullable
    public List<Task> getTasksByPart(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || userId.isEmpty()) return null;
        if (part == null || part.isEmpty()) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        return taskDAO.getTasksByPart(userId, part);
    }

    @Override
    @Nullable
    public List<Task> findTasksByStatus(@Nullable final String userId, @Nullable final Status status) {
        if (userId == null || userId.isEmpty() || status == null) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        return taskDAO.findTasksByStatus(userId, status);
    }

    @Override
    @Nullable
    public String startTask(@Nullable final String userId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        @NotNull final Date startDate = new Date();
        taskDAO.startTask(userId, taskName, startDate);
        return simpleDateFormat.format(startDate);
    }

    @Override
    @Nullable
    public String endTask(@Nullable final String userId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return null;
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        @NotNull final Date endDate = new Date();
        taskDAO.endTask(userId, taskName, endDate);
        return simpleDateFormat.format(endDate);
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return new ArrayList<>();
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        return taskDAO.findAll();
    }

    @Override
    public void persist(@Nullable final Task task) {
        if (task == null) return;
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        taskDAO.persist(task);
    }

    @Override
    public void clear() {
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        taskDAO.clear();
    }

    @Override
    public void load(@Nullable final List<Task> tasks) {
        if (tasks == null) return;
        clear();
        @Nullable final EntityManager manager = serviceLocator.getManagerFactory().createEntityManager();
        if (manager == null) return;
        @NotNull final ITaskRepository taskDAO = new TaskRepository(manager);
        for (@NotNull final Task task : tasks) {
            taskDAO.persist(task);
        }
    }
}

