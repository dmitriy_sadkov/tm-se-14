package ru.sadkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.ISessionRepository;
import ru.sadkov.tm.entity.Session;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository implements ISessionRepository {

    @NotNull
    private final EntityManager entityManager;

    public SessionRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    @Override
    public @NotNull List<Session> findByUserId(@NotNull String userId) {
        List<Session> sessions = entityManager.createQuery("SELECT session FROM Session session WHERE session.userId = ?1", Session.class)
                .setParameter(1, userId)
                .getResultList();
        return sessions;
    }

    @Override
    public boolean contains(@NotNull final Session session) {
        return entityManager.contains(session);
    }

    @Override
    public void close(@NotNull Session session) {
        entityManager.getTransaction().begin();
        entityManager.remove(session);
        entityManager.getTransaction().commit();
    }

    @Override
    public List<Session> findAll() {
        List<Session> sessions = entityManager.createQuery("SELECT session FROM Session sessions", Session.class)
                .getResultList();
        return sessions;
    }

    @Override
    public void persist(@NotNull Session session) {
        entityManager.getTransaction().begin();
        entityManager.persist(session);
        entityManager.getTransaction().commit();
    }

    @Override
    public Long isEmpty() {
        return entityManager.createQuery("SELECT COUNT(session) FROM Session session", Long.class).getSingleResult();
    }

    @Override
    public void clear() {
        entityManager.getTransaction().begin();
        entityManager.createQuery("DELETE FROM Session").executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Override
    @Nullable
    public Session findOne(@NotNull final String id) {
        return entityManager.find(Session.class, id);
    }
}
