package ru.sadkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.ITaskRepository;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.enumeration.Status;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    @NotNull
    private final EntityManager entityManager;

    public TaskRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public @NotNull List<Task> findAll(@NotNull final String userId) {
        return entityManager.createQuery("select t from Task t where t.user.id = ?1", Task.class)
                .setParameter(1, userId)
                .getResultList();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        entityManager.getTransaction().begin();
        entityManager.createQuery("delete from Task  where user.id = ?1")
                .setParameter(1, userId)
                .executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Override
    public @NotNull Boolean containsByName(@NotNull final String userId, @NotNull final String name) {
        Long count = entityManager.createQuery("SELECT COUNT(t) FROM Task t where t.name = ?1 and t.user.id = ?2", Long.class)
                .setParameter(1, name)
                .setParameter(2, userId)
                .getSingleResult();
        return count > 0;
    }

    @Override
    public @Nullable Task findOne(@NotNull final String taskId, @NotNull final String userId) {
        return entityManager.createQuery("select t from Task t where t.user.id = ?1 and t.id = ?2", Task.class)
                .setParameter(1, userId)
                .setParameter(2, taskId)
                .getSingleResult();
    }

    @Override
    public void update(@NotNull final String taskId, @NotNull final String taskName, @NotNull final String userId) {
        entityManager.getTransaction().begin();
        entityManager.createQuery("update Task set name = ?1 where user.id = ?2 and id = ?3")
                .setParameter(1, taskName)
                .setParameter(2, userId)
                .setParameter(3, taskId)
                .executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Override
    public @Nullable Task findTaskByName(@NotNull final String taskName, @NotNull final String userId) {
        return entityManager.createQuery("select t from Task t where t.user.id = ?1 and t.name = ?2", Task.class)
                .setParameter(1, userId)
                .setParameter(2, taskName)
                .getSingleResult();
    }

    @Override
    public void removeByName(@NotNull final String taskName, @NotNull final String userId) {
        entityManager.getTransaction().begin();
        entityManager.createQuery("delete from Task  where name = ?1 and user.id = ?2")
                .setParameter(1, taskName)
                .setParameter(2, userId)
                .executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Override
    public @NotNull List<Task> getTasksByPart(@NotNull final String userId, @NotNull final String part) {
        return entityManager.createQuery("select t from Task t where t.user.id = ?1 and t.name like %?2% or t.description like %?2%", Task.class)
                .setParameter(1, userId)
                .setParameter(2, part)
                .getResultList();
    }

    @Override
    public @NotNull List<Task> findTasksByStatus(@NotNull final String userId, @NotNull final Status status) {
        return entityManager.createQuery("select t from Task t where t.user.id = ?1 and t.status = ?2", Task.class)
                .setParameter(1, userId)
                .setParameter(2, status)
                .getResultList();
    }

    @Override
    public void startTask(@NotNull final String userId, @NotNull final String taskName, @NotNull final Date date) {
        entityManager.getTransaction().begin();
        entityManager.createQuery("update Task set status = ?1, dateBegin = ?2 where user.id = ?3 and name = ?4")
                .setParameter(1, Status.PROCESS)
                .setParameter(2, date)
                .setParameter(3, userId)
                .setParameter(4, taskName)
                .executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Override
    public void endTask(@NotNull final String userId, @NotNull final String taskName, @NotNull final Date date) {
        entityManager.getTransaction().begin();
        entityManager.createQuery("update Task set status = ?1, dateEnd = ?2 where user.id = ?3 and name = ?4")
                .setParameter(1, Status.DONE)
                .setParameter(2, date)
                .setParameter(3, userId)
                .setParameter(4, taskName)
                .executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Override
    public @NotNull List<Task> findAll() {
        return entityManager.createQuery("select t from Task t", Task.class)
                .getResultList();
    }

    @Override
    public void persist(@NotNull final Task task) {
        entityManager.getTransaction().begin();
        entityManager.persist(task);
        entityManager.getTransaction().commit();
    }

    @Override
    public Long isEmpty() {
        return entityManager.createQuery("SELECT COUNT(t) FROM Task t", Long.class).getSingleResult();
    }

    @Override
    public void clear() {
        entityManager.getTransaction().begin();
        entityManager.createQuery("DELETE FROM Task").executeUpdate();
        entityManager.getTransaction().commit();
    }
}
