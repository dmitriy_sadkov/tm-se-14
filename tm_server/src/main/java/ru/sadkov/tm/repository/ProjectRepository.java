package ru.sadkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IProjectRepository;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.enumeration.Status;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    @NotNull private final EntityManager entityManager;

    public ProjectRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public @NotNull List<Project> findAll(@NotNull final String userId)   {
        return entityManager.createQuery("select p from Project p where p.user.id = ?1",Project.class)
                .setParameter(1,userId)
                .getResultList();
    }

    @Override
    public @NotNull Boolean containsByName(@NotNull final String userId, @NotNull final String name) {
        Long count = entityManager.createQuery("SELECT COUNT(p) FROM Project p where p.name = ?1 and p.user.id = ?2", Long.class)
                .setParameter(1, name)
                .setParameter(2, userId)
                .getSingleResult();
        return count > 0;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String projectName)   {
        @Nullable final Project toDelete = findProjectByName(projectName,userId);
        entityManager.getTransaction().begin();
        entityManager.remove(toDelete);
        entityManager.getTransaction().commit();
    }

    @Override
    public @Nullable Project findProjectByName(@NotNull final String name, @NotNull final String userId)   {
        return entityManager.createQuery("select p from Project p where p.user.id = ?1 and p.name = ?2",Project.class)
                .setParameter(1,userId)
                .setParameter(2,name)
                .getSingleResult();
    }

    @Override
    public void removeAll(@NotNull final String userId)   {
        @NotNull final List<Project> toDelete = findAll(userId);
        entityManager.getTransaction().begin();
        for(@NotNull final Project project:toDelete){
            entityManager.remove(project);
        }
        entityManager.getTransaction().commit();
    }

    @Override
    public @Nullable Project findOne(@NotNull final String projectId, @NotNull final String userId)   {
        return entityManager.createQuery("select p from Project p where p.user.id = ?1 and p.id = ?2",Project.class)
                .setParameter(1,userId)
                .setParameter(2,projectId)
                .getSingleResult();
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String id, @NotNull final String description, @NotNull final String projectName)   {
        entityManager.getTransaction().begin();
        entityManager.createQuery("update Project set description = ?1, name = ?2 where user.id = ?3 and id = ?4")
                .setParameter(1,description)
                .setParameter(2,projectName)
                .setParameter(3,userId)
                .setParameter(4,id)
                .executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Override
    public @NotNull List<Project> findProjectsByPart(@NotNull final String userId, @NotNull final String part)   {
        return entityManager.createQuery("select p from Project p where p.user.id = ?1 and p.name like %?2% or p.description like %?2%",Project.class)
                .setParameter(1,userId)
                .setParameter(2,part)
                .getResultList();
    }

    @Override
    public void startProject(@NotNull final String userId, @NotNull final String projectName, @NotNull final Date dateBegin)   {
        entityManager.getTransaction().begin();
        entityManager.createQuery("update Project set status = ?1, dateBegin = ?2 where user.id = ?3 and name = ?4")
                .setParameter(1,Status.PROCESS)
                .setParameter(2,dateBegin)
                .setParameter(3,userId)
                .setParameter(4,projectName)
                .executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Override
    public void endProject(@NotNull final String userId, @NotNull final String projectName, @NotNull final Date dateEnd)   {
        entityManager.getTransaction().begin();
        entityManager.createQuery("update Project set status = ?1, dateEnd = ?2 where user.id = ?3 and name = ?4")
                .setParameter(1,Status.DONE)
                .setParameter(2,dateEnd)
                .setParameter(3,userId)
                .setParameter(4,projectName)
                .executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Override
    public @NotNull List<Project> findProjectsByStatus(@NotNull final String userId, @NotNull final Status status)   {
        return entityManager.createQuery("select p from Project p where p.user.id = ?1 and p.status = ?2",Project.class)
                .setParameter(1,userId)
                .setParameter(2,status)
                .getResultList();
    }

    @Override
    public @NotNull List<Project> findAll()   {
        return entityManager.createQuery("select p from Project p",Project.class)
                .getResultList();
    }

    @Override
    public void persist(@NotNull final Project project)  {
        entityManager.getTransaction().begin();
        entityManager.persist(project);
        entityManager.getTransaction().commit();
    }

    @Override
    public Long isEmpty()  {
        return entityManager.createQuery("SELECT COUNT(p) FROM Project p", Long.class).getSingleResult();
    }

    @Override
    public void clear()   {
        @NotNull final List<Project> toDelete = findAll();
        entityManager.getTransaction().begin();
        for(@NotNull final Project project:toDelete){
            entityManager.remove(project);
        }
        entityManager.getTransaction().commit();
    }
}
