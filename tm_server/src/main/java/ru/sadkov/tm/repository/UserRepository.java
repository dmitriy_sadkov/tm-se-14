package ru.sadkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IUserRepository;
import ru.sadkov.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository implements IUserRepository {

    @NotNull
    private final EntityManager entityManager;

    public UserRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public @Nullable User findOne(@NotNull final String userId) {
        return entityManager.find(User.class, userId);
    }

    @Override
    public @Nullable User findByLogin(@NotNull final String login) {
        @Nullable final User user = entityManager.createQuery("SELECT u FROM User u WHERE u.login = ?1", User.class)
                .setParameter(1, login)
                .getSingleResult();
        return user;
    }

    @Override
    public @NotNull List<User> findAll() {
        return entityManager.createQuery("SELECT u FROM User u", User.class)
                .getResultList();
    }

    @Override
    public void removeAll() {
        entityManager.getTransaction().begin();
        entityManager.createQuery("DELETE u FROM User u").executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String login) {
        entityManager.getTransaction().begin();
        entityManager.createQuery("UPDATE User  set login = ?1 where id = ?2")
                .setParameter(1, login)
                .setParameter(2, userId)
                .executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        @Nullable final User toDelete = findByLogin(login);
        if (toDelete == null) return;
        entityManager.getTransaction().begin();
        entityManager.remove(toDelete);
        entityManager.getTransaction().commit();
    }

    @Override
    public void persist(@NotNull final User user) {
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
    }

    @Override
    public Long isEmpty() {
        return entityManager.createQuery("SELECT COUNT(u) FROM User u", Long.class).getSingleResult();
    }

    @Override
    public void clear() {
        @Nullable final List<User> toDelete = findAll();
        entityManager.getTransaction().begin();
        for (@NotNull final User user : toDelete) {
            entityManager.remove(user);
        }
        entityManager.getTransaction().commit();
    }
}
