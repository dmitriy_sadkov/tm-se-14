package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.sadkov.tm.dto.ProjectDTO;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;


import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @NotNull List<ProjectDTO> convertList(@Nullable List<Project> projects);

    @NotNull ProjectDTO convert(@Nullable Project project);

    @Nullable
    String findProjectIdByName(@Nullable final String projectName, @Nullable final String userId);

    boolean persist(@Nullable final String projectName, @Nullable final String userId, @Nullable final String description);

    void removeByName(@Nullable final String projectName, @Nullable final String userId);

    @Nullable
    List<Project> findAll(@Nullable final User user);

    List<Project> getSortedProjectList(@Nullable final String userId, final Comparator<Project> comparator);

    void removeAll(@Nullable final User user);

    void update(@Nullable final String oldName, @Nullable final String newName, @Nullable final String description, @Nullable final String userId);

    @Nullable
    Project findOneByName(@Nullable final String projectName, @Nullable final User currentUser);

    @Nullable
    List<Project> findProjectsByPart(@Nullable final String id, @Nullable final String part);

    @Nullable
    List<Project> findProjectsByStatus(@Nullable final String userId, @Nullable final Status status);

    @Nullable
    String startProject(@Nullable final String userId, @Nullable final String projectName);

    @Nullable
    String endProject(@Nullable final String userId, @Nullable final String projectName);

    @NotNull
    List<Project> findAll();

    void persist(@Nullable final Project project);

    void clear();

    void load(@Nullable final List<Project> projects);
}
