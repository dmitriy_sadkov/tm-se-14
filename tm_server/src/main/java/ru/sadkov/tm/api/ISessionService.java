package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.dto.SessionDTO;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;


import java.util.List;

public interface ISessionService {

    SessionDTO open(@Nullable final String login, @Nullable final String pasword) ;

    void validate(@Nullable SessionDTO session) throws Exception;

    void validate(@Nullable final SessionDTO session, Role role) throws Exception;

    boolean isValid(@Nullable final SessionDTO session);


    void close(@Nullable SessionDTO session);

    void closeAll();

    @Nullable
    List<Session> getListSession(@Nullable final SessionDTO session);

    User getUser(@NotNull final SessionDTO session);

    void load(@NotNull final List<Session> sessions);

    @NotNull
    List<Session> findAll();
}
