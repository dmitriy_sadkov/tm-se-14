package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.dto.UserDTO;
import ru.sadkov.tm.entity.User;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface IUserService {

    void userRegister(@Nullable final String login, @Nullable final String password);

    boolean login(@Nullable final String login, @Nullable final String password);

    @Nullable
    User findOneByLogin(@Nullable final String login);


    @NotNull UserDTO convert(@Nullable User user);

    @NotNull List<UserDTO> convertList(@Nullable List<User> users);

    void addTestUsers() throws NoSuchAlgorithmException;

    @NotNull
    List<User> findAll();

    void clear();

    void userRegister(@Nullable User user);

    void userAddFromData(@Nullable final User user);

    void load(@Nullable final List<User> users);

    @Nullable
    User findOneById(@NotNull final String userId);
}
