package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.enumeration.Status;

import java.util.Date;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    List<Project> findAll(@NotNull final String userId);

    @NotNull Boolean containsByName(@NotNull String userId, @NotNull String name);

    void remove(@NotNull final String userId, @NotNull final String projectName);

    @Nullable Project findProjectByName(@NotNull String name, @NotNull String userId);

    void removeAll(@NotNull final String userId);

    @Nullable
    Project findOne(@NotNull final String projectId, @NotNull final String userId);

    void update(@NotNull final String userId, @NotNull final String id, @NotNull final String description, @NotNull final String projectName);

    @NotNull
    List<Project> findProjectsByPart(@NotNull final String userId, @NotNull final String part);


    void startProject(@NotNull final String userId, @NotNull final String projectName, @NotNull final Date dateBegin);

    void endProject(@NotNull final String userId, @NotNull final String projectName, @NotNull final Date dateEnd);

    @NotNull
    List<Project> findProjectsByStatus(@NotNull final String userId, @NotNull final Status status);


    @NotNull List<Project> findAll();
}
