package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull
    List<Session> findByUserId(@NotNull final String userId);

    boolean contains(@NotNull final Session session);

    void close(@NotNull final Session session);

    List<Session> findAll();

    @Nullable Session findOne(@NotNull String id);
}
