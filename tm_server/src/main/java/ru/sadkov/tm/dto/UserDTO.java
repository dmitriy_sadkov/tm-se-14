package ru.sadkov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.util.RandomUtil;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "users")
public final class UserDTO implements Serializable {

    @NotNull
    @Id
    @Column(name = "id", unique = true)
    private String id;

    @Nullable
    @Column(name = "login", unique = true)
    private String login;

    @Nullable
    @Column(name = "password")
    private String password;

    @Nullable
    @Enumerated(value = EnumType.STRING)
    private Role role;

    public UserDTO(@NotNull final String login, @NotNull final String password, @NotNull final Role role) {
        this.id = RandomUtil.UUID();
        this.login = login;
        this.password = password;
        this.role = role;
    }

    @Override
    @NotNull
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", login='" + login + '\'' +
                ", role=" + role +
                '}';
    }
}
