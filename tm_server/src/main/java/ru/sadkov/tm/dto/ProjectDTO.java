package ru.sadkov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.enumeration.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "projects")
public final class ProjectDTO implements Serializable {

    @NotNull
    @Id
    @Column(name = "id", unique = true)
    private String id;

    @NotNull
    @Column(name = "name")
    private String name;

    @Nullable
    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "date_create")
    @Temporal(TemporalType.DATE)
    private Date dateCreate;

    @Nullable
    @Column(name = "date_begin")
    @Temporal(TemporalType.DATE)
    private Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    @Temporal(TemporalType.DATE)
    private Date dateEnd;

    @NotNull
    @Column(name = "user_id")
    private String userId;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Status status;

    public ProjectDTO(@NotNull final String name, @NotNull final String id, @NotNull final String userId, final @NotNull String description) {
        this.name = name;
        this.id = id;
        this.userId = userId;
        this.description = description;
        this.dateCreate = new Date();
        this.dateBegin = new Date();
        this.dateEnd = new Date();
        this.status = Status.PLANNED;
    }

    public ProjectDTO(@NotNull final String name, @NotNull final String id, @NotNull final String userId) {
        this.name = name;
        this.id = id;
        this.userId = userId;
        this.description = "Description will be here";
        this.dateCreate = new Date();
        this.status = Status.PLANNED;
    }

    @Override
    @NotNull
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dateCreate=" + dateCreate +
                ", dateBegin=" + dateBegin +
                ", dateEnd=" + dateEnd +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", id='" + id + '\'' +
                '}';
    }
}
