package ru.sadkov.tm.util;

import lombok.experimental.UtilityClass;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;

import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@UtilityClass
public final class HibernateUtil {

    @NotNull
    private static final String USER = "root";
    @NotNull
    private static final String PASSWORD = "root";
    @NotNull
    private static final String URL = "jdbc:mysql://localhost:3306/app_db";
    @NotNull
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    @NotNull
    private static final String DIALECT = "org.hibernate.dialect.MySQL5InnoDBDialect";
    @NotNull
    private static final String HBM2DDL_AUTO = "update";
    @NotNull
    private static final String SHOW_SQL = "true";

    public static EntityManagerFactory factory() {
        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, DRIVER);
        settings.put(Environment.URL, URL);
        settings.put(Environment.USER, USER);
        settings.put(Environment.PASS, PASSWORD);
        settings.put(Environment.DIALECT, DIALECT);
        settings.put(Environment.HBM2DDL_AUTO, HBM2DDL_AUTO);
        settings.put(Environment.SHOW_SQL, SHOW_SQL);
        settings.put(Environment.FORMAT_SQL, "true");

        final StandardServiceRegistryBuilder registryBuilder
                = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
