package ru.sadkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.ServiceLocator;
import ru.sadkov.tm.dto.SessionDTO;
import ru.sadkov.tm.dto.UserDTO;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@WebService
public final class UserEndpoint {

    @NotNull
    private final ServiceLocator serviceLocator;

    public UserEndpoint(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @WebMethod
    public UserDTO getCurrentUser(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().convert(serviceLocator.getSessionService().getUser(session));
    }


    @WebMethod
    public void userRegister(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) {
        serviceLocator.getUserService().userRegister(login, password);
    }

    @WebMethod
    public SessionDTO login(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }


    @NotNull
    @WebMethod
    public List<UserDTO> findAllUsers(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().convertList(serviceLocator.getUserService().findAll());
    }

    @WebMethod
    public void logout(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().close(session);
    }

    @WebMethod
    public void updatePassword(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "newPassword") @Nullable final String newPassword) throws Exception, NoSuchAlgorithmException {
        serviceLocator.getSessionService().validate(session);
        //serviceLocator.getUserService().updatePassword(newPassword);
    }

    @Nullable
    @WebMethod
    public UserDTO findOneUser(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().convert(serviceLocator.getSessionService().getUser(session));
    }

    @WebMethod
    public boolean updateProfile(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "newUserName") @Nullable final String newUserName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return true;//serviceLocator.getUserService().updateProfile(newUserName);
    }

    @WebMethod
    public boolean isAuth(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getSessionService().isValid(session);
    }

    @WebMethod
    public void clearUsers(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().clear();
    }
}
