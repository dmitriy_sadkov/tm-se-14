package ru.sadkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.ServiceLocator;
import ru.sadkov.tm.dto.ProjectDTO;
import ru.sadkov.tm.dto.SessionDTO;
import ru.sadkov.tm.entity.AbstractEntity;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.enumeration.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService
public final class ProjectEndpoint {

    @NotNull
    private final ServiceLocator serviceLocator;

    public ProjectEndpoint(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @Nullable
    public List<ProjectDTO> getSortedProjectList(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "comparator") @Nullable final String comparatorName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        Comparator<Project> comparator = (Comparator<Project>) serviceLocator.getComparatorService().getComparator(comparatorName);
        return serviceLocator.getProjectService()
                .convertList(serviceLocator.getProjectService().getSortedProjectList(session.getUserId(), comparator));
    }

    @WebMethod
    @Nullable
    public String findProjectIdByName(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findProjectIdByName(projectName, session.getUserId());
    }

    @WebMethod
    public boolean createProject(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectName") @Nullable final String projectName,
            @WebParam(name = "description") @Nullable final String description) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().persist(projectName, session.getUserId(), description);
    }

    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeByName(projectName, session.getUserId());
    }

    @Nullable
    @WebMethod
    public List<ProjectDTO> findAllProjectsForUser(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService()
                .convertList(serviceLocator.getProjectService().findAll(serviceLocator.getSessionService().getUser(session)));
    }

    @WebMethod
    public void removeAllProjectsForUser(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeAll(serviceLocator.getSessionService().getUser(session));
    }

    @WebMethod
    public void updateProject(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "oldName") @Nullable final String oldName,
            @WebParam(name = "newName") @Nullable final String newName,
            @WebParam(name = "description") @Nullable final String description) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().update(oldName, newName, description, session.getUserId());
    }

    @Nullable
    @WebMethod
    public ProjectDTO findOneProjectByName(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService()
                .convert(serviceLocator.getProjectService().findOneByName(projectName, serviceLocator.getSessionService().getUser(session)));
    }


    @Nullable
    @WebMethod
    public List<ProjectDTO> findProjectsByPart(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "part") @Nullable final String part) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService()
                .convertList(serviceLocator.getProjectService().findProjectsByPart(session.getUserId(), part));
    }

    @Nullable
    @WebMethod
    public List<ProjectDTO> findProjectsByStatus(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "status") @Nullable final Status status) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService()
                .convertList(serviceLocator.getProjectService().findProjectsByStatus(session.getUserId(), status));
    }


    @Nullable
    @WebMethod
    public String startProject(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startProject(session.getUserId(), projectName);
    }

    @WebMethod
    public @Nullable String endProject(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectName") @Nullable final String projectName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().endProject(session.getUserId(), projectName);
    }

    @NotNull
    @WebMethod
    public List<ProjectDTO> findAllProjects(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService()
                .convertList(serviceLocator.getProjectService().findAll());
    }


    @WebMethod
    public void clearProjects(
            @WebParam(name = "session") @Nullable final SessionDTO session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getProjectService().clear();
    }

}
