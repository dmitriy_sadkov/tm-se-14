package ru.sadkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Session;
import ru.sadkov.tm.endpoint.SessionDTO;
import ru.sadkov.tm.exception.WrongDataException;

public final class UserLoginCommand extends AbstractCommand {

    @Override
    public String command() {
        return "login";
    }

    @Override
    public String description() {
        return "Authorize user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[AUTHORIZATION]");
        System.out.println("[ENTER LOGIN]");
        @NotNull final String login = serviceLocator.getScanner().nextLine();
        System.out.println("[ENTER PASSWORD]");
        @NotNull final String password = serviceLocator.getScanner().nextLine();
        @Nullable SessionDTO session = serviceLocator.getUserEndpoint().login(login, password);
        if(session==null)throw new WrongDataException("[SOMETHING WRONG]");
        serviceLocator.setCurrentSession(session);
        System.out.println("[YOU ARE SUCCESSFULLY LOG IN]");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
