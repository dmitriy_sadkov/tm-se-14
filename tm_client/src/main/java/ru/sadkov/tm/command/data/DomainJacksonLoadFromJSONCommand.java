package ru.sadkov.tm.command.data;

import ru.sadkov.tm.command.AbstractCommand;


public final class DomainJacksonLoadFromJSONCommand extends AbstractCommand {

    @Override
    public String command() {
        return "load-jackson-JSON";
    }

    @Override
    public String description() {
        return "Load domain from JSON with jackson";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[READING FROM JSON]");
        if(serviceLocator.getDomainEndpoint().domainLoadJacksonJSON(serviceLocator.getCurrentSession())) return;
        System.out.println("ACCESS FORBIDDEN");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
