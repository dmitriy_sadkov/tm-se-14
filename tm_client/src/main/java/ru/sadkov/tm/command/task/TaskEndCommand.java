package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Status;
import ru.sadkov.tm.endpoint.Task;
import ru.sadkov.tm.endpoint.TaskDTO;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class TaskEndCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-end";
    }

    @Override
    public String description() {
        return "End task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[END TASK]");
        System.out.println("[CHOOSE TASK TO END]");
        if (serviceLocator.getCurrentSession() == null) throw new WrongDataException("NO USER");
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskEndpoint()
                .findTasksByStatus(serviceLocator.getCurrentSession(), Status.PROCESS);
        if (tasks == null || tasks.isEmpty()) throw new WrongDataException("[NO TASKS IN PROCESS]");
        ListShowUtil.showList(tasks);
        @Nullable final String taskName = serviceLocator.getScanner().nextLine();
        if (taskName == null || taskName.isEmpty()) throw new WrongDataException("[WRONG TASK NAME]");
        @Nullable final String endDate = serviceLocator.getTaskEndpoint()
                .endTask(serviceLocator.getCurrentSession(), taskName);
        if (endDate == null || endDate.isEmpty())
            throw new WrongDataException("[ERROR! CANT'T END TASK]");
        System.out.println("[TASK: " + taskName.toUpperCase() + " ENDS]");
        System.out.println(("[END DATE: " + endDate + " ]"));
    }

    @Override
    public boolean safe() {
        return false;
    }
}
