package ru.sadkov.tm.command.data;

import ru.sadkov.tm.command.AbstractCommand;

public final class DomainDeserializeCommand extends AbstractCommand {

    @Override
    public String command() {
        return "load";
    }

    @Override
    public String description() {
        return "Load domen";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[READING FROM FILE]");
        if(serviceLocator.getDomainEndpoint().domainDeserialize(serviceLocator.getCurrentSession())) return;
        System.out.println("ACCESS FORBIDDEN");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
