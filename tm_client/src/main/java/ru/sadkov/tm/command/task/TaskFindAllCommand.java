package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.Task;
import ru.sadkov.tm.endpoint.TaskDTO;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class TaskFindAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "show-all-task";
    }

    @Override
    public String description() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[TASKS:]");
        @Nullable final List<TaskDTO> taskList = serviceLocator.getTaskEndpoint()
                .findAllTasksForUser(serviceLocator.getCurrentSession());
        if (taskList == null || taskList.isEmpty()) throw new WrongDataException("[NO TASKS]");
        ListShowUtil.showList(taskList);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
