package ru.sadkov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Project;
import ru.sadkov.tm.endpoint.ProjectDTO;
import ru.sadkov.tm.endpoint.Status;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class ProjectEndCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-end";
    }

    @Override
    public String description() {
        return "End project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[END PROJECT]");
        System.out.println("[CHOOSE PROJECT TO END]");
        if (serviceLocator.getCurrentSession() == null) throw new WrongDataException("NO USER");
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectEndpoint()
                .findProjectsByStatus(serviceLocator.getCurrentSession(), Status.PROCESS);
        if (projects == null || projects.isEmpty()) throw new WrongDataException("[NO PROJECTS IN PROCESS]");
        ListShowUtil.showList(projects);
        @Nullable final String projectName = serviceLocator.getScanner().nextLine();
        if (projectName == null || projectName.isEmpty()) throw new WrongDataException("[WRONG PROJECT NAME]");
        @Nullable final String endDate = serviceLocator.getProjectEndpoint()
                .endProject(serviceLocator.getCurrentSession(), projectName);
        if (endDate == null || endDate.isEmpty())
            throw new WrongDataException("[ERROR! CANT'T END PROJECT]");
        System.out.println("[PROJECT: " + projectName.toUpperCase() + " ENDS]");
        System.out.println(("[END DATE: " + endDate + " ]"));
    }

    @Override
    public boolean safe() {
        return false;
    }
}
