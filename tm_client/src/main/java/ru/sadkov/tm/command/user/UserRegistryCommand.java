package ru.sadkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Role;
import ru.sadkov.tm.endpoint.User;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.HashUtil;

public final class UserRegistryCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-add";
    }

    @Override
    public String description() {
        return "Register new user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REGISTRATION]");
        System.out.println("ENTER LOGIN");
        @Nullable final String login = serviceLocator.getScanner().nextLine();
        System.out.println("[ENTER PASSWORD]");
        @Nullable final String password = serviceLocator.getScanner().nextLine();
        if(login==null||login.isEmpty()||password==null||password.isEmpty())throw new WrongDataException();
        serviceLocator.getUserEndpoint().userRegister(login,password);
        System.out.println("[USER ADDED]");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
