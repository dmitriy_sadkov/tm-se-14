package ru.sadkov.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.User;
import ru.sadkov.tm.endpoint.UserDTO;
import ru.sadkov.tm.exception.WrongDataException;

public final class UserShowProfileCommand extends AbstractCommand {

    @Override
    public String command() {
        return "show-user";
    }

    @Override
    public String description() {
        return "Show current User profile";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[CURRENT USER]");
        if (serviceLocator.getCurrentSession() == null)
            throw new WrongDataException("[NO USER! PLEASE LOGIN]");
        @Nullable final UserDTO currentUser = serviceLocator.getUserEndpoint().findOneUser(serviceLocator.getCurrentSession());
        System.out.println(currentUser);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
