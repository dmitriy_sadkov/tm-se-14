package ru.sadkov.tm.command.data;

import ru.sadkov.tm.command.AbstractCommand;

public final class DomainSaveJacksonXMLCommand extends AbstractCommand {

    @Override
    public String command() {
        return "save-jackson-XML";
    }

    @Override
    public String description() {
        return "Save domain in XML with Jackson";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SAVING TO FILE]");
        if(serviceLocator.getDomainEndpoint().domainSaveJacksonXML(serviceLocator.getCurrentSession())) return;
        System.out.println("ACCESS FORBIDDEN");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
